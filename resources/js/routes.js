import Home from './components/Home'
import Login from './components/Login'

export const routes = [
    {
        path : '/',
        component : Login,
    },
    {
      path : '/home',
      component : Home,
        meta: {
          requiresAuth : true,
        }
    },
]