import {getLocalUser} from "./auth/authentication";

var user = getLocalUser();

export default{
    state : {
        welcomeMessage : 'Welcome to the Vue App BoilerPlate',
        currentUser : user,
        loginError : '',
        isLoggedIn : '',
    },
    mutations : {
        loginSuccess(state, payload){
            state.loginError = null;
            state.currentUser = Object.assign({}, payload.user, {token : payload.access_token});
            localStorage.setItem('user', JSON.stringify(state.currentUser));
            state.isLoggedIn = true;
        },
        logoutUser(state, payload){
            state.currentUser = null;
            localStorage.removeItem('user');
            state.isLoggedIn = false;
        }
    },
    actions : {},
    getters : {
        welcome(state){
            return state.welcomeMessage;
        }
    },
}