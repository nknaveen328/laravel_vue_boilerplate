export function userLogin(form){
    return new Promise((resolved, rejected) => {
       axios.post('/api/auth/login', form)
           .then(response => {
               resolved(response)
           })
           .catch(error => {
               rejected(error)
           })
    });
}

export function getLocalUser(){
    var user = localStorage.getItem('user');
    // console.log(user);

    if(!user){
        return null;
    }
    // return null;

    return JSON.parse(user);
}
